<?php

/*
 * AcceptEmail support suggested we simply check the incoming ip
 * address.
 * It is currently unclear how AcceptEmail calculates their signature
 * or even if this is done correctly.
 */
function acceptemail_is_valid_signature ($SignatureValue, $data) {
  return $_SERVER['REMOTE_ADDR'] == variable_get ('acceptemail_incoming_ip', ACCEPTEMAIL_INCOMING_IP);
/*
  $signature = base64_decode ($SignatureValue);
  $public_key = variable_get ('acceptemail_public_key', '');
  $pub_key_id = openssl_pkey_get_public (file_get_contents (drupal_get_path ('module', 'acceptemail') . '/aewspublic.cer'));
  $r = openssl_verify ($data, $signature, $pub_key_id);
*/
}

/*
 * AcceptEmail trigger to process payment notifications
 */
function acceptemail_trigger () {
  $SenderRecordReferenceID = $_REQUEST['SenderRecordReferenceID'];
  if ($SenderRecordReferenceID) {
    $data = 'RecordATID=' . $_REQUEST['RecordATID'] . '&SenderRecordReferenceID=' . $_REQUEST['SenderRecordReferenceID'];
    if (acceptemail_is_valid_signature ($_REQUEST['SignatureValue'], $data)) {
      watchdog ('acceptemail', 'Payment notification for SenderRecordReferenceID !SenderRecordReferenceID', array ('!SenderRecordReferenceID' => $SenderRecordReferenceID));
      module_invoke_all ('payment_notification', $SenderRecordReferenceID);
    }
    else {
      watchdog ('acceptemail', 'Payment received callback from invalid IP address', array (), WATCHDOG_ERROR);
    }
  }
}
