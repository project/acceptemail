<?php

define ('ACCEPTEMAIL_SOAP_MESSAGE', '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<soap:Header>%s</soap:Header>
<soap:Body>%s</soap:Body>
</soap:Envelope>');

define ('ACCEPTEMAIL_HEADER', '<AcceptEmailHeader xmlns="http://www.acceptemail.com/1.0">
<SignatureValue>%s</SignatureValue>
<Fingerprint>%s</Fingerprint>
<CreateDateTime>%s</CreateDateTime>
</AcceptEmailHeader>');

define ('ACCEPTEMAIL_SOAP_STATUS_REQUEST_FOR_EMAIL', '<EmailAddress>%s</EmailAddress>
<ReturnDetails>%s</ReturnDetails>');

define ('ACCEPTEMAIL_SOAP_CREATE_RECORD_REQUEST', '<SenderRecordReferenceID>%s</SenderRecordReferenceID>
<PaymentReference>%s</PaymentReference>
<EmailAddress>%s</EmailAddress>
<Description>%s</Description>
<Amount>
  <Value>%d</Value>
</Amount>
<EmailData>
  <KeyValuePair>
    <Key>recipientDisplayName</Key>
    <Value>%s</Value>
  </KeyValuePair>
</EmailData>');

function acceptemail_strip_white_space ($s) {
  $s2 = preg_replace ('/[[:space:]]+/', ' ', $s);
  return preg_replace ('/> </', '><', $s2);
}

/*
 * Calculate proper signature
 * @param $time
 *   Unix time.
 *
 * @return
 *   Time in Zulu format, i.e. 2006-06-15T16:31:05Z

 */
function acceptemail_time ($time) {
  return gmdate ('Y-m-d\TH:i:s\Z', $time);
}

/*
 * Calculate proper signature
 * @param $time
 *   Time formatted as Zulu time
 * @param $body
 *   XML string
 */
function acceptemail_signature ($time, $body) {
  // remove spaces from $body
  $payload = acceptemail_strip_white_space ($body) . $time;
  $private_key = variable_get ('acceptemail_private_key', '');
  $pkeyid = openssl_get_privatekey($private_key);
  // The following is similar to:
  //   echo $digest | openssl dgst –sha1 –sign private.key | openssl base64
  $ok = openssl_sign ($payload, $signature, $pkeyid);

  // free the key from memory
  openssl_free_key($pkeyid);

  if ($ok) {
    $signature = base64_encode ($signature);
  }
  else
    $signature = false;

  return $signature;
}

function acceptemail_fingerprint ($force = false) {
  $c = cache_get ('acceptemail_fingerprint');
  $fingerprint = $c ? $fingerprint = $c->data : null;
  if ($force || !$fingerprint) {
    // Fingerprint:
    // openssl x509 -fingerprint -sha1 -in public.cer -noout
    $public_key = variable_get ('acceptemail_public_key', '');

    $descriptorspec = array(
      0 => array("pipe", "r"),
      1 => array("pipe", "w"),
    );
    $process = proc_open('openssl x509 -fingerprint -sha1 -noout', $descriptorspec, $pipes);
    if (is_resource($process)) {
      fwrite($pipes[0], $public_key);
      fclose($pipes[0]);
      $fingerprint = str_replace (':', '', trim (substr (stream_get_contents($pipes[1]), 17)));
      fclose($pipes[1]);
      //$return_value = proc_close($process);
    }
    else
      $fingerprint = false;
    cache_set ('acceptemail_fingerprint', $fingerprint);
  }
  return $fingerprint;
}


function  acceptemail_header ($time, $request) {
  $zulu_time = acceptemail_time ($time);
  $signature = acceptemail_signature ($zulu_time, $request);
  $fingerprint = acceptemail_fingerprint ();
  $header = sprintf (ACCEPTEMAIL_HEADER, $signature, $fingerprint, $zulu_time);
  return $header;
}

/*
 * Sent request $request_name with contents $request
 *
 * @return
 *   empty array or array with ErrorCode/ErrorDescription
 */
function acceptemail_soap_call ($request_name, $request) {
  $time = time();
  $full_request = '<' . $request_name . 'Request xmlns="http://www.acceptemail.com/1.0">' . "\n" . $request . "\n</" . $request_name . 'Request>';
  $header = acceptemail_header ($time, $full_request);
  $message = sprintf (ACCEPTEMAIL_SOAP_MESSAGE, $header, $full_request);

  //print (htmlspecialchars ($message));
  $url = variable_get ('acceptemail_url', ACCEPTEMAIL_APPLICATION_URL);

  $ch = curl_init ($url);
  curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=utf-8"));
  curl_setopt ($ch, CURLOPT_POST, 1);
  curl_setopt ($ch, CURLOPT_POSTFIELDS, $message);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec ($ch);
  $info = curl_getinfo($ch);
  //print (htmlspecialchars ($response));
  $xml = new SimpleXMLElement($response);
  $xml->registerXPathNamespace('ae', 'http://www.acceptemail.com/1.0');
  $responses = $xml->xpath('//ae:' . $request_name . 'Response');
  if ($responses)
    $response = $responses[0];
  if (isset ($response->Error)) {
    $result['ErrorCode'] = (string) $response->Error->ErrorCode;
    $result['ErrorDescription'] = (string) $response->Error->ErrorDescription;
    watchdog ('acceptemail', $result['ErrorDescription'], null, WATCHDOG_ERROR);
  }
  else
    $result = array ();
  // retry if ErrorCode is AEWS_GEN0000 ?

  return $result;
}