<?php

/*
 * Admin form
 */
function acceptemail_admin_settings($form, &$form_state) {
  // Restrict administration of this module
  if (!user_access('administer acceptemail')) {
    $form["error"] = array(
      '#type' => 'item',
      '#title' => t('You are not authorized to access the acceptemail settings.'),
    );
    return system_settings_form($form);
  }

  $form['general']['acceptemail_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Private key'),
    '#default_value' => variable_get ('acceptemail_private_key', ''),
    '#cols' => 70,
  );
  $form['general']['acceptemail_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Public key'),
    '#default_value' => variable_get ('acceptemail_public_key', ''),
    '#description' => t('Note: the public key must be <a href="https://application.acceptemail.com/IDealSettings.aspx">uploaded to AcceptEmail.</a>'),
    '#cols' => 70,
  );
  $form['general']['acceptemail_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Application url'),
    '#default_value' => variable_get ('acceptemail_url', ACCEPTEMAIL_APPLICATION_URL),
  );
  $form['general']['acceptemail_incoming_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('Incoming IP'),
    '#default_value' => variable_get ('acceptemail_incoming_ip', ACCEPTEMAIL_INCOMING_IP),
    '#description' => 'The IP address from the AcceptEmail server that sends payment received triggers.',
  );

  $form['general']['generate-keys'] = array(
    '#type' => 'submit',
    '#value' => t('Generate new private and public keys'),
    '#submit' => array ('acceptemail_generate_self_signed_certificate'),
  );

  return system_settings_form($form);
}
